const selectDiv = document.querySelector('#selectDiv');
const requestLogsDiv = document.querySelector("#requestLogs");
const fetchTxsBtn = document.querySelector('button#fetchTxsBtn');
const downloadCSVBtn = document.querySelector('button#downloadCSV');

const collapseBtn = document.querySelector('button#collapseBtn');
const collapseAddDeviceDiv = document.querySelector('#collapseAddDeviceDiv');

const addSingleBtn = document.querySelector('button#addSingleBtn');
const csvDevicesFileInput = document.querySelector('input#csvDevicesFileInput');
const addBulkBtn = document.querySelector('button#addBulkBtn');

const delDeviceBtn = document.querySelector('button#delDeviceBtn');

const fetchTxsForAllBtn = document.querySelector('button#fetchTxsForAllBtn');

const fetchAlertsDiv = document.querySelector('#fetchingTxs-alerts');
const addingDAlerts = document.querySelector('#addingD-alerts');
const tableSingle = document.querySelector("table#outputSingleDevice");
const tableBulk = document.querySelector("table#outputAlldevices");
const avgDiv = document.querySelector("#avg");
const totalTokensDiv = document.querySelector("#totalTokens");

const logOutBtn = document.querySelector('button#logOutBtn');
const hideSecBtn = document.querySelector('button#hideSecBtn');
const header = document.querySelector('#header');

const alertTime = 3500;
const loadinDevicesTime = 1000;
const realAmountDenominator = 1000000; // assetDecimals = 6
const addressRole = "receiver"; // the role of the address in txs.
const assetID = 27165954; // Planets 
let csv;

function createSelect(devices) {
    const select = document.createElement('select');
    select.setAttribute("id", "select-device")
    select.setAttribute("class", "form-select form-select-lg mb-3")
    select.setAttribute("aria-label", ".form-select-lg")
    const firstOption = document.createElement('option');
    firstOption.innerText = `select a device`
    select.append(firstOption)

    devices.map(dv => {
        const option = document.createElement('option');
        option.setAttribute('name', dv.name)
        option.setAttribute('address', dv.address)
        option.innerText = dv.name
        select.append(option)
    })
    return select
};

async function fetchTxsAPI(api) {
    let response = await fetch(api)
    return response
}

function isDatesValid() {
    const from = document.querySelector('input[id="from"]').value;
    const to = document.querySelector('input[id="to"]').value;
    if (!from || !to) {
        showAlert("Please select from and to date", fetchAlertsDiv)
        return false
    }
    return true
};

function disableBtns() {
    fetchTxsBtn.disabled = true;
    fetchTxsBtn.style.cursor = "not-allowed";

    fetchTxsForAllBtn.style.cursor = "not-allowed";
    fetchTxsForAllBtn.disabled = true;

    document.querySelector('select#select-device').disabled = true;
};

function enableBtns() {
    fetchTxsBtn.style.cursor = "pointer";
    fetchTxsBtn.disabled = false;
    fetchTxsBtn.innerHTML = `<i class="bi bi-collection-fill"></i> Fetch Transactions`;

    fetchTxsForAllBtn.style.cursor = "pointer";
    fetchTxsForAllBtn.disabled = false;
    fetchTxsForAllBtn.innerHTML = `<i class="bi bi-collection-fill"></i> Fetch Transactions for all devices`;
    document.querySelector('select#select-device').disabled = false;

}

async function getSingleDTxs() {
    empty()
    const selectedOption = document.querySelector('select#select-device').selectedOptions[0];
    let address = selectedOption.getAttribute('address');

    if (!address) {
        showAlert("Please select a device", fetchAlertsDiv)
        return;
    }
    const deviceName = selectedOption.getAttribute('name');

    const _isDatesValid = isDatesValid()
    if (!_isDatesValid) return;

    disableBtns()
    appenSpinner(fetchTxsBtn)

    const transactionList = await getTxs(deviceName, address)
    enableBtns()

    if (!transactionList.length) return

    let daytoAmountMap = mapTxsToTokensPerDay(transactionList);
    console.log("daytoAmountMap", daytoAmountMap);

    createTable(daytoAmountMap, deviceName);
    generateCSV(daytoAmountMap, deviceName);
    downloadCSVBtn.hidden = false;
    calculateTokensAvg_Sum(transactionList);
    window.scrollBy(0, window.innerHeight);
};

function empty(type = "single") {
    if (type != "all") requestLogsDiv.innerHTML = ""
    tableSingle.innerHTML = "";
    tableBulk.innerHTML = "";
    csv = "";
    downloadCSVBtn.hidden = true;
    avg.innerHTML = ""
    totalTokensDiv.innerHTML = ""
}

async function getTxs(deviceName, address, type = "single") {
    const from = document.querySelector('input[id="from"]').value;
    const to = document.querySelector('input[id="to"]').value;

    let afterTime = new Date(from).toISOString() // min - included
    let beforeTime = new Date(to).toISOString() //  max - excluded

    let transactionList = [];

    let nextPage = "firstPage";

    while (nextPage) {

        const baseAPIURL = "https://algoindexer.algoexplorerapi.io/v2/";
        const transactionsURL = `${baseAPIURL}transactions?address=${address}&limit=1000&asset-id=${assetID}&address-role=${addressRole}&after-time=${afterTime}&before-time=${beforeTime}${nextPage == "firstPage" ? "" : `&next=${nextPage}`}`

        const response = await fetchTxsAPI(transactionsURL);
        if (response.status != 200) showAlert(response.message, fetchAlertsDiv);

        const data = await response.json();

        const { transactions } = data;

        if (transactions.length) {
            transactionList = transactionList.concat(...transactions);
            transactionList = transactionList.filter(tx => {
                const day = new Date(parseInt(`${tx["round-time"]}000`)).toLocaleDateString()
                const maxTxDate = new Date(to).toLocaleDateString();
                return day != maxTxDate
            });


            if (transactionList.length && type != "all") {
                const maxTxDate = getDateString(transactions[0]["round-time"])
                const minTxDate = getDateString(transactions[transactions.length - 1]["round-time"])
                logRequestStatus("success", deviceName, `Tokens earned from ${minTxDate} to ${maxTxDate}`)
            }
        }

        nextPage = data["next-token"];
    }

    console.log("transactionList", transactionList)

    if (transactionList.length) return transactionList;

    if (!transactionList.length) {
        if (type != "all") logRequestStatus("warning", deviceName, `No txs done ${from == to ? `on ${new Date(from).toLocaleDateString()}` : ` from ${new Date(from).toLocaleDateString()} to ${new Date(to).toLocaleDateString()}`}`)
        return []
    }
};

function showAlert(alertText, alertDiv, alertType = "warning") {
    const alert = document.createElement('div');
    const i = document.createElement('i')
    if (alertType == "warning") i.setAttribute("class", "bi bi-exclamation-triangle-fill");
    else i.setAttribute("class", "bi bi-info-square-fill");
    alert.append(i)
    alert.setAttribute("class", `alert alert-${alertType}`)
    alert.setAttribute("role", "alert")
    alert.append(` ${alertText}.`)
    if (alertDiv.id == "fetchingTxs-alerts") {
        alertDiv.append(alert);
    } else {
        alertDiv.insertBefore(alert, alertDiv.firstChild);
    }
    setTimeout(() => alertDiv.innerHTML = "", alertTime)
};

function logRequestStatus(status, deviceName, message) {
    const header = document.createElement('h5');

    const span = document.createElement('span');
    span.setAttribute("class", `badge text-bg-${status}`)
    span.innerText = deviceName;

    header.append(span);
    header.append(` ${message}`);
    requestLogsDiv.append(header)
};

function appenSpinner(btn) {
    const span = document.createElement('span');
    span.setAttribute("class", "spinner-border spinner-border-sm")
    span.setAttribute("role", "status")
    span.setAttribute("aria-hidden", "true")
    btn.innerHTML = "";
    btn.append(span);
    btn.append(" Fetching Transactions...");
};

function getDateString(roundTime) {
    return new Date(parseInt(`${roundTime}000`)).toLocaleDateString()
};

function mapTxsToTokensPerDay(txs) {
    const daytoAmountMap = new Map();
    txs.forEach(tx => {
        const day = new Date(parseInt(`${tx["round-time"]}000`)).toLocaleDateString()
        const amount = tx["asset-transfer-transaction"]["amount"]

        if (daytoAmountMap.has(day)) {
            daytoAmountMap.set(day, daytoAmountMap.get(day) + amount)
        }
        else {
            daytoAmountMap.set(day, amount)
        }
    });

    for (const [day, amount] of daytoAmountMap.entries()) {
        daytoAmountMap.set(day, amount / realAmountDenominator);
    }

    return daytoAmountMap;
};

function generateTableRowsHead(table, deviceName) {
    let thead = table.createTHead();

    let rowName = thead.insertRow();
    rowName.setAttribute("class", "table-primary");

    let nameCell = document.createElement("th");
    nameCell.appendChild(document.createTextNode('Device Name'));
    rowName.appendChild(nameCell);

    let nameValueCell = document.createElement("th");
    nameValueCell.appendChild(document.createTextNode(deviceName));
    rowName.appendChild(nameValueCell);

    let row = thead.insertRow();
    row.setAttribute('id', 'tableHead')
    let dateCell = document.createElement("th");
    dateCell.appendChild(document.createTextNode('Date'));
    row.appendChild(dateCell);

    let numberCell = document.createElement("th");
    numberCell.appendChild(document.createTextNode('Tokens received'));
    row.appendChild(numberCell);
};

function createTableHeadCell(text) {
    let cell = document.createElement("th");
    cell.appendChild(document.createTextNode(text));
    return cell
};

function appendTableHead(table, cells) {
    let thead = table.createTHead();
    let row = thead.insertRow();
    cells = cells.map(cell => createTableHeadCell(cell))
    console.log(cells)
    cells.forEach(cell => row.appendChild(cell))
};

function appendCellToTableBodyRow(row, text) {
    let cell = row.insertCell();
    cell.append(document.createTextNode(text))
    row.appendChild(cell)
};

function appendRowToTableBody(tableBody, cells) {
    let row = tableBody.insertRow();
    if (cells[1] == 0 || cells[2] == 0) row.classList.add('zero-tokens');
    cells.forEach(cell => appendCellToTableBodyRow(row, cell))
};

function createTable(map, deviceName) {
    generateTableRowsHead(tableSingle, deviceName)
    let tbody = tableSingle.createTBody();

    for (const [day, tokens] of map.entries()) {
        let row = tbody.insertRow();
        if (tokens == 0) row.classList.add('zero-tokens');

        let dayCell = row.insertCell();
        dayCell.append(document.createTextNode(day))
        let tokensCell = row.insertCell();
        tokensCell.append(document.createTextNode(tokens))
    }
};

function generateCSV(map, deviceName) {
    csv += `${deviceName}\nDate,Tokens\n`
    for (const [day, tokens] of map.entries()) {
        if (tokens != 0) csv += `${day},${tokens}\n`
    };
    csv = csv.trim()
};

function downloadCSV() {
    if (!csv) return;
    const deviceName = csv.split('\n')[0].trim();
    csv = csv.replace(deviceName, "").trim();

    csv = csv.replace(/\n/g, "\r\n");
    var blob = new Blob([csv], { type: "text/plain" });
    var anchor = document.createElement("a");
    anchor.download = `${deviceName}.csv`;
    anchor.href = window.URL.createObjectURL(blob);
    anchor.target = "_blank";
    anchor.style.display = "none";
    document.body.appendChild(anchor);
    anchor.click();
    document.body.removeChild(anchor);
}

function collapse() {
    if (collapseBtn.classList.contains("collapsed")) {
        collapseBtn.classList.remove("collapsed")
        collapseBtn.setAttribute("aria-expanded", "false")
        collapseAddDeviceDiv.classList.remove("show")
    } else {
        collapseBtn.classList.add("collapsed")
        collapseBtn.setAttribute("aria-expanded", "true")
        collapseAddDeviceDiv.classList.add("show")
    }
};

async function sendDevice(name, address) {
    const response = await fetch(`${window.location.origin}/devices`, {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        method: "POST",
        body: JSON.stringify({ "name": name.trim(), "address": address })
    });

    const data = await response.json();
    if (response.status == 200) {
        const { message } = data;
        showAlert(message, addingDAlerts, 'success');
        await fetchDevices();
    };

    if (response.status != 200) {
        const { errors } = data;
        errors.forEach(error => showAlert(error["error"], addingDAlerts))
    }
};

async function addSingle() {
    const dInputName = document.querySelector('input#dInputName').value;
    let dInputAddress = document.querySelector('input#dInputAddress').value;
    if (!dInputName || !dInputAddress) return;
    dInputAddress = dInputAddress.replace('https://algoexplorer.io/address/', "").trim();
    if (!(dInputName.replace("PW", ""))) showAlert(`Name can't be empty`, addingDAlerts);
    if (!dInputAddress) showAlert(`Address can't be empty`, addingDAlerts);
    if (dInputAddress) await sendDevice(dInputName, dInputAddress)
};

async function addBulk() {
    if (!csvDevicesFileInput.files.length) {
        showAlert(`Please choose a csv file first`, addingDAlerts);
        return
    }
    var reader = new FileReader();
    reader.readAsText(csvDevicesFileInput.files[0]);
    reader.onload = function (e) {
        const devices = e.target.result.trim().split('\n');
        devices.forEach(async (device) => {
            const [name, address] = device.split(',')
            await sendDevice(name, address.replace('https://algoexplorer.io/address/', "").trim())
        })
    }
};

async function fetchDevices() {
    const response = await fetch(`${window.location.origin}/devices`);
    let devices = await response.json();

    if (devices.length) {
        devices = devices.sort((a, b) => parseInt(a.name.match(/\d+/)[0]) - parseInt(b.name.match(/\d+/)[0]))
        setTimeout(() => {
            const select = createSelect(devices);
            selectDiv.innerHTML = "";
            selectDiv.append(select);
        }, loadinDevicesTime)
    };
};

async function deleteDevice() {
    const devices = []
    const name = document.querySelector('input#deletedDName').value;
    if (!name) {
        showAlert(`Device's name can't be empty`, addingDAlerts);
        return
    };

    if (name.includes(",")) name.split(',').forEach(deviceName => { if (deviceName) devices.push(deviceName) })
    else devices.push(name)

    for (let index = 0; index < devices.length; index++) {
        const name = devices[index];

        const response = await fetch(`${window.location.origin}/devices`, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            method: "DELETE",
            body: JSON.stringify({ "name": name.trim() })
        });

        const data = await response.json();

        if (response.status == 200) {
            const { message } = data;
            showAlert(message, addingDAlerts, 'danger');
            await fetchDevices();
        };

        if (response.status != 200) {
            const { error } = data;
            showAlert(error, addingDAlerts)
        }
    }
};

async function calculateTokensForAll() {
    empty("all");
    const _isDatesValid = isDatesValid()
    if (!_isDatesValid) return;
    disableBtns()
    appenSpinner(fetchTxsForAllBtn);

    let totalTokens = 0;
    let devices = Array.from(document.querySelector("select").options).map(option => {
        return { "name": option.getAttribute("name"), "address": option.getAttribute("address") }
    });

    devices.splice(0, 1);

    appendTableHead(tableBulk, ['Device', 'Total tokens', 'Average tokens']);
    const tbody = tableBulk.createTBody();
    const noDays = getNoDays();

    for (let index = 0; index < devices.length; index++) {
        const { name, address } = devices[index];
        const transactions = await getTxs(name, address, "all");
        let totalTokensPerD = 0;
        if (transactions && transactions.length) {
            transactions.forEach(tx => {
                totalTokens += tx["asset-transfer-transaction"]["amount"]
                totalTokensPerD += tx["asset-transfer-transaction"]["amount"]
            });
            totalTokensPerD = totalTokensPerD / realAmountDenominator;
            appendRowToTableBody(tbody, [name, totalTokensPerD, (totalTokensPerD / noDays).toFixed(3)]);
            window.scrollBy(0, window.innerHeight);
            tableBulk.scrollBy(0, tableBulk.scrollHeight);
            requestLogsDiv.innerHTML = `Fetched transactions of ${index + 1} devices out of ${devices.length}`
        } else {
            appendRowToTableBody(tbody, [name, 0, 0]);
        }
    }
    enableBtns()
    totalTokensDiv.innerHTML = ""
    totalTokensDiv.innerHTML = `<div class="card"><div class="card-body">Total tokens = ${totalTokens / realAmountDenominator}</div></div>`;
    window.scrollBy(0, window.innerHeight);
};

function getNoDays() {
    let from = new Date(document.querySelector('input[id="from"]').value)
    let to = new Date(document.querySelector('input[id="to"]').value);

    const diffTime = Math.abs(to - from);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    return diffDays
};

function calculateTokensAvg_Sum(transactions) {
    let totalTokens = 0;
    const noDays = getNoDays();
    transactions.forEach(tx => totalTokens += tx["asset-transfer-transaction"]["amount"]);
    let avg = (totalTokens / realAmountDenominator) / noDays;
    avgDiv.innerHTML = `<div class="card"><div class="card-body">Average Tokens = ${(avg).toFixed(3)}</div></div>`;
    totalTokensDiv.innerHTML = `<div class="card"><div class="card-body">Total tokens = ${totalTokens / realAmountDenominator}</div></div>`;
};

function logOut() {
    document.cookie = "token=";
    window.location.href = `${window.location.origin}/login`
    window.location.assign(`${window.location.origin}/login`)
};

function hideHSec() {
    if (header.style.display == "none") {
        header.style.display = "block"
    } else {
        header.style.display = "none"
    }
};

(async () => {
    await fetchDevices();

    fetchTxsBtn.addEventListener('click', getSingleDTxs);
    downloadCSVBtn.addEventListener('click', downloadCSV);
    collapseBtn.addEventListener('click', collapse);
    addSingleBtn.addEventListener('click', async () => {
        await addSingle()
    });

    addBulkBtn.addEventListener('click', async () => {
        await addBulk()
    });

    delDeviceBtn.addEventListener('click', async () => {
        await deleteDevice()
    });

    fetchTxsForAllBtn.addEventListener('click', async () => {
        await calculateTokensForAll()
    });

    logOutBtn.addEventListener('click', logOut);
    hideSecBtn.addEventListener('click', hideHSec);
})()