const express = require('express');
var cookieParser = require('cookie-parser')
const path = require('path');
const Device = require('./mongodb/device');
const User = require('./mongodb/user');
const connectDB = require('./mongodb/db.js');
require('dotenv').config();

process.on("unhandledRejection", error => {
   console.error(error)
});

(async () => {
   await connectDB()
})();

const port = process.env.PORT || 5000;

var app = express();
app.disable('x-powered-by');
app.use(express.json());
app.use(express.static(path.join(__dirname, 'web-app/public/')));
app.use(cookieParser())

app.use(function (req, res, next) {
   console.log(`${req.method} ${req.url}`)
   next()
});


const isAuth = async (req, passwordToBeVerified = "", type = "") => {
   let token = "";
   if (type == "verify") token = passwordToBeVerified;
   else token = req.cookies["token"];
   let user = await User.findOne({ "username": "admin" }).select(["password", "-_id"]);
   if (user) password = user["password"]

   if (!token || token != user["password"]) return false;
   if (token == user["password"]) return true
};

app.get('/', (req, res) => {
   res.redirect('login')
});

app.get('/login', async (req, res) => {
   const isUserAuth = await isAuth(req)
   if (!isUserAuth) res.sendFile(path.join(__dirname, '/web-app/public/login.html'))
   if (isUserAuth) res.redirect('tokens-report.html')
});

app.get('/change-password', async (req, res) => {
   const isUserAuth = await isAuth(req);
   if (!isUserAuth) res.status(403).sendFile(path.join(__dirname, '/web-app/403-forbidden.html'))
   if (isUserAuth) res.sendFile(path.join(__dirname, '/web-app/change-password.html'))
});

app.post('/change-password', async (req, res) => {
   const isUserAuth = await isAuth(req);
   if (!isUserAuth) res.status(403).send({ "error": "You are not authenticated, please login again" });

   const { currentPassword, newPassword, confirmPassword } = req.body;
   const isCurrentPasswordVerified = await isAuth(req, currentPassword, "verify");
   if (currentPassword == newPassword) res.status(403).send({ "error": "new password match current password" })
   if (!isCurrentPasswordVerified) res.status(403).send({ "error": "Curret password is wrong" })
   if (newPassword != confirmPassword) res.status(403).send({ "error": "New password must match the confirm password" });

   const { nModified, ok } = await User.updateOne({ "username": 'admin' }, { "password": newPassword });
   if (nModified == 1 && ok == 1) res.cookie('token', newPassword).send({ "message": 'Password has been successfully changed' })
});

app.post("/tokens-report.html", async (req, res) => {
   const { password } = req.body;
   const isCurrentPasswordVerified = await isAuth(req, password, "verify");
   if (!isCurrentPasswordVerified) res.status(403).send({ "error": `Wrong Password` });
   if (isCurrentPasswordVerified) res.cookie('token', password).redirect('tokens-report.html')
});

app.get("/tokens-report.html", async (req, res) => {
   const isUserAuth = await isAuth(req)
   if (!isUserAuth) res.status(403).sendFile(path.join(__dirname, '/web-app/403-forbidden.html'))
   if (isUserAuth) res.sendFile(path.join(__dirname, '/web-app/tokens-report.html'))
});

app.get('/devices', async (req, res) => {
   const devices = await Device.find({}).select(["name", "address", "-_id"]);
   res.send(JSON.stringify(devices))
});

app.post('/devices', async (req, res) => {
   const { name, address } = req.body;

   const isNameDup = await Device.findOne({ "name": name });
   const isAddressDup = await Device.findOne({ "address": address });

   const errors = [];
   if (isNameDup) errors.push({ "error": `Duplicate name : ${name}, it's already in the list` })
   if (isAddressDup) errors.push({ "error": `Duplicate Address: ${address}, it's already in the list` })

   if (errors.length) res.status(400).send({ errors })

   if (!isNameDup && !isAddressDup) {
      const isCreated = await Device.create(req.body);
      if (isCreated) res.send({ "message": `The device #${name} has been successfully added` })
   }
});

app.delete('/devices', async (req, res) => {
   const { name } = req.body;
   const { deletedCount } = await Device.deleteOne({ "name": name });

   if (deletedCount == 0) res.status(400).send({ "error": `Device #${name} does not exist` });
   if (deletedCount == 1) res.send({ "message": `The device #${name} has been successfully deleted` })
});

app.listen(port, () => {
   console.log(`tokens-report-generation-tool is listening at http://localhost:${port}`)
});